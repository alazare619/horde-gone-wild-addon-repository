
SkadaDB = {
	["namespaces"] = {
		["LibDualSpec-1.0"] = {
			["char"] = {
				[""] = {
					["profile"] = "2020",
					["enabled"] = false,
					["specGroup"] = 1,
				},
			},
		},
	},
	["hasUpgraded"] = true,
	["profiles"] = {
		["2020"] = {
			["modules"] = {
				["notankwarnings"] = true,
				["ccannounce"] = true,
			},
			["windows"] = {
				{
					["barslocked"] = true,
					["background"] = {
						["height"] = 165,
					},
					["wipemode"] = "Healing",
					["y"] = 27.99989891052246,
					["barfont"] = "Expressway",
					["name"] = "Skada Damage",
					["mode"] = "Damage",
					["spark"] = false,
					["barwidth"] = 414.9999694824219,
					["point"] = "BOTTOMRIGHT",
					["barfontsize"] = 12,
					["modeincombat"] = "Damage",
					["x"] = -1.999267578125,
				}, -- [1]
			},
			["modulesBlocked"] = {
				["Deaths"] = false,
				["CC"] = false,
				["Power"] = false,
				["Healing"] = false,
				["DamageTaken"] = false,
				["Enemies"] = false,
				["Overhealing"] = false,
				["Interrupts"] = false,
				["TotalHealing"] = false,
				["Debuffs"] = false,
				["Dispels"] = false,
				["Threat"] = false,
			},
			["icon"] = {
				["hide"] = true,
			},
			["report"] = {
				["number"] = 25,
				["channel"] = "instance_chat",
				["target"] = "",
				["mode"] = "",
			},
			["columns"] = {
				["Threat_TPS"] = true,
			},
			["tooltiprows"] = 6,
			["setstokeep"] = 30,
			["onlykeepbosses"] = true,
		},
	},
}
