
ElvDB = {
	["faction"] = {
	},
	["profileKeys"] = {
	},
	["gold"] = {
	},
	["namespaces"] = {
		["LibDualSpec-1.0"] = {
		},
	},
	["class"] = {
	},
	["global"] = {
		["general"] = {
			["smallerWorldMap"] = false,
			["showMissingTalentAlert"] = true,
			["eyefinity"] = true,
		},
		["uiScale"] = "1.0",
		["unitframe"] = {
			["aurafilters"] = {
				["Blacklist"] = {
					["spells"] = {
						[145389] = {
							["enable"] = true,
							["priority"] = 0,
							["stackThreshold"] = 0,
						},
						[5487] = {
							["enable"] = true,
							["priority"] = 0,
							["stackThreshold"] = 0,
						},
						[130609] = {
							["enable"] = true,
							["priority"] = 0,
							["stackThreshold"] = 0,
						},
						[40120] = {
							["enable"] = true,
							["priority"] = 0,
							["stackThreshold"] = 0,
						},
						[783] = {
							["enable"] = true,
							["priority"] = 0,
							["stackThreshold"] = 0,
						},
						[768] = {
							["enable"] = true,
							["priority"] = 0,
							["stackThreshold"] = 0,
						},
					},
				},
			},
			["effectiveHealthSpeed"] = 0.5,
			["effectivePowerSpeed"] = 0.5,
		},
		["profileCopy"] = {
			["bags"] = {
				["bagBar"] = false,
				["general"] = false,
				["split"] = false,
				["vendorGrays"] = false,
				["cooldown"] = false,
			},
		},
		["nameplates"] = {
			["filters"] = {
				["ElvUI_Totem"] = {
				},
				["ElvUI_Boss"] = {
				},
				["ElvUI_NonTarget"] = {
				},
				["ElvUI_Target"] = {
				},
				["Boss"] = {
				},
			},
		},
	},
	["profiles"] = {
		["2020 Tank"] = {
			["databars"] = {
				["reputation"] = {
					["enable"] = true,
					["hideInCombat"] = true,
				},
			},
			["currentTutorial"] = 9,
			["general"] = {
				["totems"] = {
					["spacing"] = 2,
					["size"] = 31,
				},
				["autoTrackReputation"] = true,
				["backdropfadecolor"] = {
					["a"] = 0.800000011920929,
					["r"] = 0.05882352941176471,
					["g"] = 0.05882352941176471,
					["b"] = 0.05882352941176471,
				},
				["valuecolor"] = {
					["r"] = 0.96,
					["g"] = 0.55,
					["b"] = 0.73,
				},
				["stickyFrames"] = 1,
				["autoRoll"] = true,
				["font"] = "Expressway",
				["fontSize"] = 13,
				["autoAcceptInvite"] = true,
				["autoRepair"] = "PLAYER",
				["minimap"] = {
					["locationFont"] = "Expressway",
				},
				["bottomPanel"] = false,
				["interruptAnnounce"] = "EMOTE",
			},
			["enhanced"] = {
				["map"] = {
					["fogClear"] = {
						["enable"] = true,
					},
				},
				["general"] = {
					["undressButton"] = true,
					["altBuyMaxStack"] = true,
					["showQuestLevel"] = true,
					["autoRepChange"] = true,
					["trainAllSkills"] = true,
				},
				["equipmentSet"] = {
					["enable"] = true,
				},
			},
			["bags"] = {
				["countFontSize"] = 13,
				["itemLevelFontSize"] = 13,
				["countFont"] = "Expressway",
				["vendorGrays"] = {
					["enable"] = true,
				},
				["itemLevelFont"] = "Expressway",
			},
			["hideTutorial"] = 1,
			["auras"] = {
				["font"] = "Expressway",
				["debuffs"] = {
					["countFontSize"] = 13,
					["durationFontSize"] = 13,
				},
				["buffs"] = {
					["countFontSize"] = 13,
					["durationFontSize"] = 13,
				},
			},
			["movers"] = {
				["ElvUF_FocusCastbarMover"] = "BOTTOMRIGHTElvUIParentBOTTOMRIGHT-251348",
				["BottomBarMover"] = "TOPElvUIParentTOP292-489",
				["PetAB"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-4,-229",
				["ElvUF_RaidMover"] = "TOPLEFT,ElvUIParent,TOPLEFT,4,-83",
				["LeftChatMover"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,4,4",
				["GMMover"] = "TOPLEFT,ElvUIParent,TOPLEFT,4,-4",
				["BuffsMover"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-213,-3",
				["ElvUF_Raid10Mover"] = "TOPLEFTElvUIParentTOPLEFT397-214",
				["BossButton"] = "BOTTOM,ElvUIParent,BOTTOM,0,145",
				["LootFrameMover"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,302,465",
				["ZoneAbility"] = "BOTTOM,ElvUIParent,BOTTOM,0,225",
				["ElvUF_RaidpetMover"] = "TOPLEFT,ElvUIParent,BOTTOMLEFT,4,1080",
				["VehicleSeatMover"] = "BOTTOMRIGHT,ElvUIParent,BOTTOMRIGHT,-99,277",
				["ElvUF_FocusMover"] = "BOTTOMRIGHTElvUIParentBOTTOMRIGHT-251381",
				["ElvUIBagMover"] = "BOTTOMRIGHT,ElvUIParent,BOTTOMRIGHT,-4,26",
				["TimeManagerFrameMover"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-4,-204",
				["ClassBarMover"] = "BOTTOM,ElvUIParent,BOTTOM,0,423",
				["MicrobarMover"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,4,215",
				["ElvUF_PartyMover"] = "TOPLEFT,ElvUIParent,TOPLEFT,4,-83",
				["ElvUF_Raid25Mover"] = "TOPLEFTElvUIParentTOPLEFT4-201",
				["ElvUF_PetMover"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,289,264",
				["ExperienceBarMover"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,426,34",
				["WatchFrameMover"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-4,-203",
				["ElvUF_TargetMover"] = "BOTTOMRIGHT,ElvUIParent,BOTTOMRIGHT,-420,229",
				["ElvBar_Pet"] = "BOTTOM,ElvUIParent,BOTTOM,0,80",
				["LossControlMover"] = "BOTTOM,ElvUIParent,BOTTOM,0,131",
				["ElvUF_Raid40Mover"] = "TOPLEFT,ElvUIParent,BOTTOMLEFT,4,721",
				["ElvUF_PlayerCastbarMover"] = "BOTTOM,ElvUIParent,BOTTOM,0,396",
				["RaidMarkerBarAnchor"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,40,235",
				["ElvAB_1"] = "BOTTOM,ElvUIParent,BOTTOM,0,4",
				["ElvAB_2"] = "BOTTOM,ElvUIParent,BOTTOM,0,38",
				["ElvAB_4"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,429,4",
				["ArenaHeaderMover"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-292,-330",
				["TalkingHeadFrameMover"] = "TOP,ElvUIParent,TOP,0,-4",
				["AltPowerBarMover"] = "BOTTOM,ElvUIParent,BOTTOM,0,103",
				["AzeriteBarMover"] = "BOTTOMRIGHT,ElvUIParent,BOTTOMRIGHT,-435,20",
				["ElvAB_3"] = "BOTTOMRIGHT,ElvUIParent,BOTTOMRIGHT,-434,4",
				["ElvAB_5"] = "BOTTOMRIGHT,ElvUIParent,BOTTOMRIGHT,-429,4",
				["ArtifactBarMover"] = "BOTTOMRIGHT,ElvUIParent,BOTTOMRIGHT,-426,34",
				["ElvUF_PlayerMover"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,420,229",
				["PlayerPowerBarMover"] = "BOTTOMElvUIParentBOTTOM0429",
				["ObjectiveFrameMover"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-98,-205",
				["BNETMover"] = "TOPElvUIParentTOP0-4",
				["ShiftAB"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,421,302",
				["ElvAB_6"] = "BOTTOMRIGHT,ElvUIParent,BOTTOMRIGHT,-4,223",
				["ReputationBarMover"] = "BOTTOMRIGHT,ElvUIParent,BOTTOMRIGHT,-426,19",
				["ElvUF_TargetCastbarMover"] = "BOTTOM,ElvUIParent,BOTTOM,0,435",
				["TooltipMover"] = "BOTTOMRIGHT,ElvUIParent,BOTTOMRIGHT,-4,211",
				["ElvUF_TankMover"] = "TOPLEFT,ElvUIParent,BOTTOMLEFT,4,1127",
				["BossHeaderMover"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-255,-330",
				["TotemBarMover"] = "BOTTOMLEFT,ElvUIParent,BOTTOMLEFT,429,80",
				["ElvUF_TargetTargetMover"] = "BOTTOMRIGHT,ElvUIParent,BOTTOMRIGHT,-289,264",
				["RightChatMover"] = "BOTTOMRIGHT,ElvUIParent,BOTTOMRIGHT,-4,4",
				["AlertFrameMover"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-295,-222",
				["DebuffsMover"] = "TOPRIGHT,ElvUIParent,TOPRIGHT,-213,-151",
				["ElvUF_AssistMover"] = "TOPLEFTElvUIParentBOTTOMLEFT41101",
			},
			["tooltip"] = {
				["headerFontSize"] = 13,
				["healthBar"] = {
					["font"] = "Expressway",
					["height"] = 10,
				},
				["textFontSize"] = 13,
				["font"] = "Expressway",
				["smallTextFontSize"] = 13,
			},
			["unitframe"] = {
				["fontSize"] = 14,
				["font"] = "Expressway",
				["units"] = {
					["tank"] = {
						["targetsGroup"] = {
							["enable"] = false,
						},
					},
					["focustarget"] = {
						["enable"] = true,
					},
					["targettarget"] = {
						["debuffs"] = {
							["enable"] = false,
						},
						["threatStyle"] = "GLOW",
						["portrait"] = {
							["camDistanceScale"] = 1,
						},
						["infoPanel"] = {
							["height"] = 12,
						},
					},
					["focus"] = {
						["debuffs"] = {
							["priority"] = "Blacklist,Warlock,Personal,RaidDebuffs,Dispellable,Whitelist",
						},
						["castbar"] = {
							["height"] = 25,
						},
					},
					["target"] = {
						["debuffs"] = {
							["sizeOverride"] = 25,
							["sortDirection"] = "ASCENDING",
							["priority"] = "Boss,Blacklist,Personal,RaidDebuffs,CCDebuffs,Friendly:Dispellable",
							["numrows"] = 2,
							["perrow"] = 6,
							["attachTo"] = "FRAME",
							["maxDuration"] = 0,
						},
						["customTexts"] = {
							["altpower"] = {
								["yOffset"] = -16,
								["font"] = "ElvUI Alt-Font",
								["justifyH"] = "CENTER",
								["fontOutline"] = "MONOCHROMEOUTLINE",
								["enable"] = true,
								["xOffset"] = 0,
								["text_format"] = "[altpower:percent]",
								["size"] = 17,
							},
						},
						["name"] = {
							["position"] = "LEFT",
							["xOffset"] = 5,
							["text_format"] = "[namecolor][name:long][difficultycolor] [smartlevel][shortclassification]",
						},
						["height"] = 73,
						["buffs"] = {
							["sizeOverride"] = 25,
							["anchorPoint"] = "TOPLEFT",
							["priority"] = "Boss,Blacklist,Personal,nonPersonal",
							["numrows"] = 2,
							["perrow"] = 6,
						},
						["aurabar"] = {
							["enable"] = false,
							["attachTo"] = "FRAME",
							["friendlyAuraType"] = "HARMFUL",
							["enemyAuraType"] = "HELPFUL",
						},
						["middleClickFocus"] = false,
						["power"] = {
							["attachTextTo"] = "Power",
							["yOffset"] = 4,
							["position"] = "CENTER",
							["xOffset"] = 0,
						},
						["width"] = 300,
						["health"] = {
							["xOffset"] = -5,
						},
						["castbar"] = {
							["width"] = 250,
							["height"] = 24,
						},
					},
					["boss"] = {
						["debuffs"] = {
							["sizeOverride"] = 30,
							["yOffset"] = 0,
							["priority"] = "Blacklist,Personal,RaidDebuffs",
							["numrows"] = 1,
							["perrow"] = 5,
						},
						["width"] = 220,
						["health"] = {
							["text_format"] = "[healthcolor][health:percent]",
						},
						["height"] = 47,
						["buffs"] = {
							["sizeOverride"] = 19,
							["enable"] = false,
							["priority"] = "Boss,Blacklist,CastByUnit,Whitelist,RaidBuffsElvUI",
						},
					},
					["raid40"] = {
						["showPlayer"] = false,
						["GPSArrow"] = {
							["size"] = 40,
						},
						["name"] = {
							["position"] = "LEFT",
							["xOffset"] = 153,
						},
						["height"] = 15,
						["verticalSpacing"] = 2,
						["horizontalSpacing"] = 4,
						["rdebuffs"] = {
							["font"] = "Expressway",
							["size"] = 26,
						},
						["numGroups"] = 1,
						["growthDirection"] = "DOWN_RIGHT",
						["roleIcon"] = {
							["enable"] = true,
							["position"] = "LEFT",
						},
						["raidRoleIcons"] = {
							["position"] = "TOPRIGHT",
						},
						["groupsPerRowCol"] = 8,
						["buffIndicator"] = {
							["enable"] = false,
						},
						["width"] = 150,
					},
					["raidpet"] = {
						["groupsPerRowCol"] = 8,
					},
					["assist"] = {
						["enable"] = false,
					},
					["raid"] = {
						["showPlayer"] = false,
						["GPSArrow"] = {
							["enable"] = false,
						},
						["name"] = {
							["position"] = "LEFT",
							["xOffset"] = 153,
							["text_format"] = "[namecolor][name:medium]",
						},
						["height"] = 20,
						["verticalSpacing"] = 5,
						["raidicon"] = {
							["yOffset"] = 0,
						},
						["horizontalSpacing"] = 4,
						["rdebuffs"] = {
							["font"] = "Expressway",
						},
						["growthDirection"] = "DOWN_RIGHT",
						["roleIcon"] = {
							["position"] = "LEFT",
						},
						["power"] = {
							["enable"] = false,
						},
						["width"] = 150,
						["groupsPerRowCol"] = 5,
					},
					["player"] = {
						["debuffs"] = {
							["enable"] = false,
							["anchorPoint"] = "LEFT",
							["perrow"] = 4,
						},
						["name"] = {
							["position"] = "RIGHT",
							["xOffset"] = -5,
							["text_format"] = "[namecolor][name:medium]",
						},
						["height"] = 73,
						["buffs"] = {
							["attachTo"] = "FRAME",
						},
						["aurabar"] = {
							["enable"] = false,
							["attachTo"] = "FRAME",
						},
						["RestIcon"] = {
							["enable"] = false,
						},
						["power"] = {
							["attachTextTo"] = "Power",
							["yOffset"] = 4,
							["position"] = "CENTER",
							["xOffset"] = 0,
						},
						["castbar"] = {
							["width"] = 250,
							["height"] = 24,
						},
						["width"] = 300,
						["health"] = {
							["xOffset"] = 5,
							["text_format"] = "[healthcolor][health:current]",
						},
						["classbar"] = {
							["detachFromFrame"] = true,
							["additionalPowerText"] = false,
						},
					},
					["party"] = {
						["debuffs"] = {
							["sizeOverride"] = 20,
							["enable"] = false,
							["priority"] = "Blacklist,Boss,RaidDebuffs,CCDebuffs,Dispellable",
						},
						["GPSArrow"] = {
							["enable"] = false,
						},
						["infoPanel"] = {
							["height"] = 12,
						},
						["name"] = {
							["xOffset"] = 152,
							["text_format"] = "[namecolor][name:short]",
						},
						["startFromCenter"] = true,
						["height"] = 20,
						["verticalSpacing"] = 5,
						["raidicon"] = {
							["yOffset"] = 0,
						},
						["horizontalSpacing"] = 4,
						["rdebuffs"] = {
							["enable"] = true,
							["font"] = "Expressway",
						},
						["numGroups"] = 4,
						["growthDirection"] = "DOWN_RIGHT",
						["groupBy"] = "ROLE",
						["roleIcon"] = {
							["yOffset"] = -1,
							["position"] = "LEFT",
							["xOffset"] = 1,
						},
						["raidWideSorting"] = true,
						["power"] = {
							["enable"] = false,
							["text_format"] = "",
							["yOffset"] = 2,
						},
						["groupsPerRowCol"] = 4,
						["health"] = {
							["position"] = "BOTTOM",
							["text_format"] = "[healthcolor][health:deficit]",
							["yOffset"] = 2,
						},
						["orientation"] = "MIDDLE",
						["width"] = 200,
					},
				},
				["smoothbars"] = true,
				["colors"] = {
					["auraBarBuff"] = {
						["r"] = 0.96,
						["g"] = 0.55,
						["b"] = 0.73,
					},
					["healthclass"] = true,
					["customhealthbackdrop"] = true,
					["transparentPower"] = true,
					["colorhealthbyvalue"] = false,
					["health_backdrop"] = {
						["r"] = 0.0705882352941177,
						["g"] = 0.0705882352941177,
						["b"] = 0.0705882352941177,
					},
					["castClassColor"] = true,
					["power"] = {
						["RAGE"] = {
							["r"] = 0.780392156862745,
							["g"] = 0.250980392156863,
							["b"] = 0.250980392156863,
						},
					},
					["castColor"] = {
						["r"] = 0.1,
						["g"] = 0.1,
						["b"] = 0.1,
					},
					["transparentCastbar"] = true,
					["health"] = {
						["r"] = 0.1,
						["g"] = 0.1,
						["b"] = 0.1,
					},
					["transparentAurabars"] = true,
				},
				["fontOutline"] = "OUTLINE",
			},
			["datatexts"] = {
				["fontSize"] = 13,
				["panelTransparency"] = true,
				["panels"] = {
					["LeftMiniPanel"] = "Coords",
					["RightChatDataPanel"] = {
						["middle"] = "Bags",
					},
					["LeftChatDataPanel"] = {
						["right"] = "Talent/Loot Specialization",
						["left"] = "Combat Time",
					},
					["RightMiniPanel"] = "Time",
				},
				["font"] = "Expressway",
			},
			["actionbar"] = {
				["bar3"] = {
					["heightMult"] = 2,
					["buttons"] = 12,
					["mouseover"] = true,
					["buttonsize"] = 36,
				},
				["fontOutline"] = "OUTLINE",
				["font"] = "Expressway",
				["barPet"] = {
					["point"] = "BOTTOMRIGHT",
					["buttonspacing"] = 1,
					["backdrop"] = false,
					["buttonsPerRow"] = 10,
					["buttonsize"] = 21,
				},
				["bar6"] = {
					["enabled"] = true,
					["mouseover"] = true,
					["buttonsPerRow"] = 1,
					["backdrop"] = true,
				},
				["microbar"] = {
					["enabled"] = true,
					["mouseover"] = true,
				},
				["bar2"] = {
					["enabled"] = true,
					["widthMult"] = 3,
					["buttonsize"] = 36,
				},
				["bar5"] = {
					["enabled"] = false,
					["buttons"] = 10,
					["mouseover"] = true,
					["point"] = "TOPLEFT",
					["buttonsPerRow"] = 5,
					["buttonsize"] = 36,
				},
				["fontSize"] = 13,
				["bar1"] = {
					["point"] = "TOPLEFT",
					["paging"] = {
						["ROGUE"] = "",
					},
					["buttonsize"] = 36,
					["widthMult"] = 3,
				},
				["macrotext"] = true,
				["stanceBar"] = {
					["buttonsPerRow"] = 9,
					["buttonsize"] = 25,
				},
				["cooldown"] = {
					["checkSeconds"] = true,
					["fonts"] = {
						["enable"] = true,
						["font"] = "Expressway",
					},
					["override"] = true,
				},
				["bar4"] = {
					["mouseover"] = true,
					["point"] = "BOTTOMLEFT",
					["buttonsPerRow"] = 6,
					["buttonsize"] = 36,
				},
			},
			["nameplates"] = {
				["fontSize"] = 12,
				["lowHealthThreshold"] = 0.2,
				["units"] = {
					["ENEMY_PLAYER"] = {
						["debuffs"] = {
							["filters"] = {
								["priority"] = "CCDebuffs,Warlock,Blacklist,blockNoDuration,Personal,Boss,RaidDebuffs",
							},
						},
					},
					["ENEMY_NPC"] = {
						["debuffs"] = {
							["filters"] = {
								["priority"] = "Blacklist,Personal,CCDebuffs,RaidDebuffs",
							},
						},
						["eliteIcon"] = {
							["enable"] = true,
						},
					},
				},
				["font"] = "Expressway",
			},
			["addOnSkins"] = {
				["skada"] = {
					["template"] = "NONE",
					["titleTemplate"] = "Transparent",
				},
				["embed"] = {
					["leftWindowWidth"] = 105,
				},
			},
			["cooldown"] = {
				["checkSeconds"] = true,
				["enable"] = false,
			},
			["chat"] = {
				["tabFont"] = "Expressway",
				["font"] = "Expressway",
				["panelWidth"] = 423,
				["emotionIcons"] = false,
				["fade"] = false,
				["timeStampFormat"] = "%H:%M:%S ",
				["numAllowedCombatRepeat"] = 10,
				["panelHeight"] = 210,
			},
		},
	},
}