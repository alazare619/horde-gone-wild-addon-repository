
OneRing_Config = {
	["ProfileStorage"] = {
		["default"] = {
			["SliceBinding"] = true,
			["Bindings"] = {
				["RaidSymbols"] = "T",
				["OPieTracking"] = false,
				["WorldMarkers"] = "G",
				["OPieAutoQuest"] = false,
				["CommonTrades"] = false,
				["ladinBuffs"] = "B",
				["MountRing"] = "V",
				["PaladinTools"] = false,
			},
			["GhostMIRings"] = false,
		},
		["2020"] = {
			["SliceBinding"] = true,
			["Bindings"] = {
				["CommonTrades"] = false,
				["PaladinTools"] = false,
				["RaidSymbols"] = "T",
				["WorldMarkers"] = "G",
				["MountRing"] = "V",
				["OPieAutoQuest"] = false,
				["DKCombat"] = "F11",
				["ladinBuffs"] = "B",
				["OPieTrinkets"] = "R",
				["OPieTracking"] = false,
			},
			["GhostMIRings"] = false,
		},
	},
	["PersistentStorage"] = {
		["RingKeeper"] = {
			["OPieDeletedRings"] = {
				["PaladinAuras"] = true,
			},
			["ladinBuffs"] = {
				{
					["sliceToken"] = "ABuejAB2oSc",
					["id"] = 20217,
				}, -- [1]
				{
					["sliceToken"] = "ABuejAB2oSx",
					["id"] = 19740,
				}, -- [2]
				{
					["sliceToken"] = "ABuejAB2oS5",
					["id"] = 25780,
				}, -- [3]
				{
					["sliceToken"] = "ABuejAB2oSz",
					["id"] = 20154,
				}, -- [4]
				{
					["sliceToken"] = "ABuejAB2oSl",
					["id"] = 31801,
				}, -- [5]
				{
					["sliceToken"] = "ABuejAB2oSk",
					["id"] = 20165,
				}, -- [6]
				{
					"item", -- [1]
					86569, -- [2]
					["sliceToken"] = "ABuejNpg1ir",
				}, -- [7]
				["limit"] = "PALADIN",
				["save"] = true,
				["name"] = "Paladin Buffs",
			},
			["MountRing"] = {
				{
					["sliceToken"] = "ABuejFe48ig",
					["id"] = 23509,
				}, -- [1]
				{
					["sliceToken"] = "ABuejFe48id",
					["id"] = 61447,
				}, -- [2]
				{
					["sliceToken"] = "ABuejFe48i3",
					["id"] = 148396,
				}, -- [3]
				{
					["sliceToken"] = "ABuejNpg1iy",
					["id"] = 43927,
				}, -- [4]
				["hotkey"] = "ALT-M",
				["save"] = true,
				["name"] = "Mount Ring",
			},
			["OPieFlagStore"] = {
				["FlushedDefaultColors"] = true,
			},
		},
	},
}
