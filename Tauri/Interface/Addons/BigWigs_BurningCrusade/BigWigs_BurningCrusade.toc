## Interface: 50400
## X-Curse-Packaged-Version: r33-release
## X-Curse-Project-Name: BigWigs_BurningCrusade
## X-Curse-Project-ID: bigwigs_burningcrusade
## X-Curse-Repository-ID: wow/bigwigs_burningcrusade/mainline

## Title: Big Wigs [|cffeda55fBurning Crusade|r]
## Notes: A collection of Big Wigs modules from the Burning Crusade era.

## LoadOnDemand: 1
## Dependencies: BigWigs

## X-Category: Raid
## X-BigWigs-LoadOn-ZoneId: 775, 780, 779, 776, 799, 782
## X-BigWigs-LoadOn-WorldBoss: 473, 17711, 465, 18728

modules.xml

