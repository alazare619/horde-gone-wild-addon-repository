local E, L, V, P, G = unpack(ElvUI)
local M = [[Interface\AddOns\ElvUI_AddOnSkins\Media\]]

E.AddonSkinsMedia = {
	Textures = {
		Horde = M..[[Textures\Horde.blp]],
		Alliance = M..[[Textures\Alliance.blp]]
	}
}